class AddJobStatusToBhpbForms < ActiveRecord::Migration
  def change
    add_column :bhpb_forms, :job_status, :string
  end
end
