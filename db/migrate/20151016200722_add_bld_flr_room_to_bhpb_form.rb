class AddBldFlrRoomToBhpbForm < ActiveRecord::Migration
  def change
    add_column :bhpb_forms, :building, :string
    add_column :bhpb_forms, :floor, :string
    add_column :bhpb_forms, :room, :string
  end
end
