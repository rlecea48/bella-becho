class AddPaymentMethodToBhpbForm < ActiveRecord::Migration
  def change
    add_column :bhpb_forms, :payment_method, :string
  end
end
