class AddPurchasOrderToBhpbForm < ActiveRecord::Migration
  def change
    add_column :bhpb_forms, :purchase_order, :string
  end
end
