class CreateBhpbForms < ActiveRecord::Migration
  def change
    create_table :bhpb_forms do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :category_type
      t.string :activity
      t.string :csg
      t.string :department
      t.string :summary
      t.text :description
      t.string :priority
      t.string :target_date
      t.string :delivery_opt
      t.string :job_ref
      t.string :quantity
      t.string :size
      t.string :cc_email
      t.timestamps
    end
  end
end
