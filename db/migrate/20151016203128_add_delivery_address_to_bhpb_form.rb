class AddDeliveryAddressToBhpbForm < ActiveRecord::Migration
  def change
    add_column :bhpb_forms, :delivery_address, :string
  end
end
