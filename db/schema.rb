# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151123173433) do

  create_table "bhpb_forms", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "category_type"
    t.string   "activity"
    t.string   "csg"
    t.string   "department"
    t.string   "summary"
    t.text     "description"
    t.string   "priority"
    t.string   "target_date"
    t.string   "delivery_opt"
    t.string   "job_ref"
    t.string   "quantity"
    t.string   "size"
    t.string   "cc_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "building"
    t.string   "floor"
    t.string   "room"
    t.string   "payment_method"
    t.string   "delivery_address"
    t.string   "purchase_order"
    t.string   "job_status"
  end

  create_table "users", force: true do |t|
    t.string   "email",            null: false
    t.string   "crypted_password"
    t.string   "salt"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "phone"
    t.string   "csg"
    t.string   "dept"
    t.string   "user_type"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
