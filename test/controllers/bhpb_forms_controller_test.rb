require 'test_helper'

class BhpbFormsControllerTest < ActionController::TestCase
  setup do
    @bhpb_form = bhpb_forms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bhpb_forms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bhpb_form" do
    assert_difference('BhpbForm.count') do
      post :create, bhpb_form: { activity: @bhpb_form.activity, category_type: @bhpb_form.category_type, cc_email: @bhpb_form.cc_email, csg: @bhpb_form.csg, delivery_opt: @bhpb_form.delivery_opt, department: @bhpb_form.department, description: @bhpb_form.description, email: @bhpb_form.email, job_ref: @bhpb_form.job_ref, name: @bhpb_form.name, phone: @bhpb_form.phone, priority: @bhpb_form.priority, quantity: @bhpb_form.quantity, size: @bhpb_form.size, summary: @bhpb_form.summary, target_date: @bhpb_form.target_date }
    end

    assert_redirected_to bhpb_form_path(assigns(:bhpb_form))
  end

  test "should show bhpb_form" do
    get :show, id: @bhpb_form
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bhpb_form
    assert_response :success
  end

  test "should update bhpb_form" do
    patch :update, id: @bhpb_form, bhpb_form: { activity: @bhpb_form.activity, category_type: @bhpb_form.category_type, cc_email: @bhpb_form.cc_email, csg: @bhpb_form.csg, delivery_opt: @bhpb_form.delivery_opt, department: @bhpb_form.department, description: @bhpb_form.description, email: @bhpb_form.email, job_ref: @bhpb_form.job_ref, name: @bhpb_form.name, phone: @bhpb_form.phone, priority: @bhpb_form.priority, quantity: @bhpb_form.quantity, size: @bhpb_form.size, summary: @bhpb_form.summary, target_date: @bhpb_form.target_date }
    assert_redirected_to bhpb_form_path(assigns(:bhpb_form))
  end

  test "should destroy bhpb_form" do
    assert_difference('BhpbForm.count', -1) do
      delete :destroy, id: @bhpb_form
    end

    assert_redirected_to bhpb_forms_path
  end
end
