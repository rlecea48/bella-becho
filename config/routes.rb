Rails.application.routes.draw do
  
  root :to => 'user_sessions#new'
  resources :users
  resources :user_sessions
  resources :bhpb_forms
  
  
  get 'login' => 'user_sessions#new', :as => :login
  post 'logout' => 'user_sessions#destroy', :as => :logout
  get 'send_mail', to: 'bhpb_forms#send_mail'

end
