json.array!(@bhpb_forms) do |bhpb_form|
  json.extract! bhpb_form, :id, :name, :email, :phone, :category_type, :activity, :csg, :department, :summary, :description, :priority, :target_date, :delivery_opt, :job_ref, :quantity, :size, :cc_email
  json.url bhpb_form_url(bhpb_form, format: :json)
end
