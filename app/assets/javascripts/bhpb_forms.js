jQuery(document).ready(function($) {
		// Chained Select for Category Type >> Activity
        $('#bhpb_form_category_type').change(function() {
                $('select#bhpb_form_activity').find('option').remove();
                var graphicsOptions = ["Create New Job", "Digital Signage", "File Conversion", "LMS Training Modules", "Photography", "Update Existing Job"]
                var printingAndDuplicating = ["Binders/tabs","Business Card Scanning","CD Duplication","Printing Large Format","Printing Small Format","Scan Image"]
                var category = $('select[name="bhpb_form[category_type]"]').val();



                if(category == "Graphics"){

                        $('select#bhpb_form_activity').find('option').remove();
                        $(graphicsOptions).each(function(index, value) {
                                $('select#bhpb_form_activity').append($("<option></option>").attr("value",value).text(value));
                        });

                } else if (category == "Printing and Duplicating") {

                        $('select#bhpb_form_activity').find('option').remove();
                        $(printingAndDuplicating).each(function(index, value) {
                                $('select#bhpb_form_activity').append($("<option></option>").attr("value",value).text(value));
                        });
                }
        });

		// DISPLAY DELIVERY BOXES FOR delivery_address option
		
		$('#bhpb_form_delivery_opt').change(function() {
			var delivery_option = $('select[name="bhpb_form[delivery_opt]"]').find('option:selected').attr("name");
			$('div#delivery_address').hide();
			$('div#mailroom_carrier_options').hide();
			
			if(delivery_option == "courier" || delivery_option == "mail") {
				$('div#delivery_address').show();
			} else if (delivery_option == "mailroom") {
				$('div#delivery_address').hide();
				$('div#mailroom_carrier_options').show();
			} else {
				$('div#delivery_address').hide();
				$('div#mailroom_carrier_options').hide();
			}


		});

		// DISPLAY DELIVERY BOXES FOR delivery_address option
		
		$('#bhpb_form_payment_method').change(function() {
			var payment_option = $('select[name="bhpb_form[payment_method]"]').find('option:selected').val();
			$('#po_field').hide();
			if(payment_option == "PO") {
				$('#po_field').show();
			} else {
				$('#po_field').hide();
			}

		});
});


