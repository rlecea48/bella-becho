require 'ntlm/smtp'
class UserMailer < ActionMailer::Base
    default from: 'noreply@bellabecho.com'
    @url  = 'http://bhpb.bellabecho.com'

	def user_email(recipient)
		@bhpb_form = BhpbForm.last    
		mail(to: recipient, subject: "BHPB Graphics Request Confirmation Request # "+ (@bhpb_form.id+1944).to_s)
	end
	def welcome_email(user)
		@bhpb_user = user
		@url  = 'http://bhpb.bellabecho.com'
	    mail(to: @bhpb_user.email, subject: 'Welcome to bhpb.bellabecho.com')

	end
	def report_email
		@bhpb_user = 'bhpb.bellabecho.com'
		@url  = 'http://bhpb.bellabecho.com'
	    mail(to: 'noreply@bellabecho.com' , subject: 'new user added')
	end
end


