class UsersController < ApplicationController
  before_filter :require_login, only: [:show, :edit, :update]     
  #before_filter :authenticate, only: [:index, :new, :destroy, :create] 
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.all
  end
  # GET /users/1
  # GET /users/1.json
  def show
  end
  # GET /users/new
  def new
    @user = User.new
  end
  # GET /users/1/edit
  def edit
  end
  
  def create
   @user = User.new(user_params)
  
      respond_to do |format|
        if @user.save
          format.html { redirect_to new_user_session_path}
          flash[:notice] = "User was successfully created"
          format.json { render :show, status: :created, location: @user }
          user = @user 
          send_registration(user)
        else
          format.html { render :new }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    
  end
  def send_registration(user)
    
    UserMailer.welcome_email(user).deliver
    
  end  
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to new_bhpb_form_path, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to new_user_session_path, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation,:name,:phone,:csg,:dept)
    end
end
