class UserSessionsController < ApplicationController

  def new
    flash[:notice] = "First time users must register before logging in."

  end

  def create
  	@user = login(params[:email], params[:password])
    if @user
      redirect_to new_bhpb_form_path, :notice => "You are Logged in"
    else
      redirect_to root_url, :alert => "Invalid Username or Password"
    end
  end

  def destroy
   logout
    redirect_to(root_url, notice: 'You are Logged out')	
  end
end
