class BhpbFormsController < ApplicationController
  before_filter :require_login
  before_filter :auth, only: [:index,:edit,:destroy] 
  before_action :set_bhpb_form, only: [:show, :edit, :update, :destroy]
  def auth
    if current_user.user_type != "admin"
      logout
    end    
  end
  def index
     @bhpb_forms = BhpbForm.order(:created_at)
      respond_to do |format|
        format.html
        format.csv {send_data @bhpb_forms.as_csv,filename: "Bhpb_#{Date.today}.csv"}
      end
  end
  def show
  end
  def new
    @bhpb_form = BhpbForm.new
  end
  def edit
  end
  def create
    @bhpb_form = BhpbForm.new(bhpb_form_params)
    respond_to do |format|
      if @bhpb_form.save
          
        format.html {redirect_to @bhpb_form}
        flash[:notice] = "Graphic Request Form was successfully submitted"
        format.json { render :show, status: :created, location: @bhpb_form }
        send_mail
      
      else
        format.html { render :new }
        format.json { render json: @bhpb_form.errors, status: :unprocessable_entity }
      end
    end
  end

  def send_mail
    recipient = BhpbForm.last.email.to_s unless BhpbForm.last.nil?
    UserMailer.user_email(recipient).deliver unless BhpbForm.last.nil?
      swpp_agent = ["deborah@bellabecho.com","louise@bellabecho.com","relecea48@yahoo.com"]
      swpp_agent.each do |email|
        UserMailer.user_email(email).deliver
      end
        cc_list = BhpbForm.last.cc_email.split(',') unless BhpbForm.last.cc_email.length < 1
        cc_list = "" if BhpbForm.last.cc_email.length < 1
        if !cc_list.nil? && cc_list != ""
      cc_list.each do |mailman|
           UserMailer.user_email(mailman).deliver
      end
    end
  end  
  def update
    respond_to do |format|
      if @bhpb_form.update(bhpb_form_params)
        format.html { redirect_to bhpb_forms_url }
        format.json { render :index, status: :ok, location: @bhpb_forms }
      else
        format.html { render :index }
        format.json { render json: @bhpb_form.errors, status: :unprocessable_entity }
      end
    end
  end
  def destroy
    @bhpb_form.destroy
    respond_to do |format|
      format.html { redirect_to bhpb_forms_url, notice: 'Graphic Request Form was successfully removed.' }
      format.json { head :no_content }
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bhpb_form
      @bhpb_form = BhpbForm.find(params[:id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def bhpb_form_params
      params.require(:bhpb_form).permit(:name, :email, :phone, :category_type, :activity, :csg, :department, :summary, :description, :priority, :target_date, 
        :delivery_opt, :job_ref, :quantity, :size, :cc_email, :building, :floor, :room, :payment_method, :delivery_address, :purchase_order, :job_status)
    end
end
