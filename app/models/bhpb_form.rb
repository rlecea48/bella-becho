class BhpbForm < ActiveRecord::Base
        validates :name, presence: true, length: { maximum: 25 }
        validates :email, presence: true
        validates :phone, presence: true, length: { minimum: 3 }
        validates :category_type, presence: true
        validates :activity, presence: true
        validates :csg, presence: true
        validates :department, presence: true
        validates :summary, presence: true
        validates :target_date, presence: true
        validates :quantity, presence: true, length: { minimum: 1 }
        def self.as_csv
          CSV.generate do |csv|
            csv << ["id", "status", "name", "email", "phone", "category_type", "activity", "csg", "department", "summary",
              "description", "priority", "target_date", "delivery_opt", "job_ref", "quantity", "size", "cc_email",
               "building", "floor", "room", "payment_method", "delivery_address", "purchase_order","created date", "updated date"]
            all.each do |item|
              if item.id.to_i != 60 && item.id.to_i != 67 && item.id.to_i != 68 && item.id.to_i >= 139
                   csv << [item.id.to_i + 1944, item.job_status, item.name, item.email, item.phone, item.category_type, item.activity, item.csg, item.department, item.summary,
                                item.description, item.priority, item.target_date, item.delivery_opt, item.job_ref, item.quantity,item. size, item.cc_email,
                                item.building, item.floor, item.room, item.payment_method, item.delivery_address, item.purchase_order, item.created_at, item.updated_at]
             end
             if item.id.to_i != 60 && item.id.to_i != 67 && item.id.to_i != 68 && item.id.to_i < 139
                   csv << [item.id.to_i + 1975, item.job_status, item.name, item.email, item.phone, item.category_type, item.activity, item.csg, item.department, item.summary,
                                item.description, item.priority, item.target_date, item.delivery_opt, item.job_ref, item.quantity,item. size, item.cc_email,
                                item.building, item.floor, item.room, item.payment_method, item.delivery_address, item.purchase_order, item.created_at, item.updated_at]
             end
          end
end
end
end

